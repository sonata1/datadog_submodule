locals {
  log_router_definition = {
    name      = "log_router"
    essential = true
    image     = var.log_router_image
    user      = "0"
    firelensConfiguration = {
      type = "fluentbit"
      options = {
        enable-ecs-log-metadata = "true"
        config-file-type = "file"
        config-file-value = "/fluent-bit/configs/parse-json.conf"
      }
    }
  }
}
