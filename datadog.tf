locals {
  datadog_definition = {
    name      = "datadog"
    cpu       = var.datadog_cpu
    essential = true,
    image     = var.datadog_image
    memory    = var.datadog_memoryReservation
    logConfiguration = {
      "logDriver"     = "awslogs"
      "secretOptions" = null
      "options" = {
        "awslogs-group"         = var.datadog_logs_group
        "awslogs-region"        = var.region
        "awslogs-stream-prefix" = "ecs"
        "awslogs-create-group"  = "true"
      }
    },
    environment = [
      {
        name = "DD_SITE"
        value = "datadoghq.eu"
      },
      {
        name = "DD_TAGS"
        value = join(" ", [for k, v in var.datadog_tags : join(":", [k, v])])
      },
      {
        name = "ECS_FARGATE"
        value = "true"
      }
    ],
    secrets = [
      {
        name = "DD_API_KEY"
        valueFrom = var.datadog_api_key
      }
    ]
  }
}
