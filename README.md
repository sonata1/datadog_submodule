# fargate_with_datadog

This codebase deploys the Terrafrom managed resources for fargate_with_datadog

## Usage

### Environments

This codebase will deploy the same Terraform codebase into multiple environments (AWS accounts) you can select which `environment` you are deploying into by setting the `ENVIRONMENT` envvar. This defaults to `test`.

### AWS Authentication

Authenticate yourself using one of the following methods:

- Set AWS environment variables.
- Use a profile from your credentials file and set `AWS_PROFILE`.

### Available Commands

| Command | Description |
|---|---|
| `make init` | Equivalent to `terraform init`, runs `make clean` before init. |
| `make plan` | Equivalent to `terraform plan` |
| `make apply` | Equivalent to `terraform apply` |
| `make check` | Equivalent to `terrafrom fmt -diff -check` |
| `make fmt` | Equivalent to `terraform fmt` |
| `make console` | Equivalent to `terraform console`. Requires `make init` first |
| `make shell` | Will drop you into the docker container in `/terraform`. Does not send your environment so you will need to set your AWS auth inside the container |
| `make clean` | Clean up any Terraform files hanging about |

### Options

These are rarely used options, mostly for debugging.

| ENVAR | Value | Description |
|---|---|---|
| `NO_BACKEND`| `1` | Do not use the backend file in `backends/` |
| `NO_VAR`| `1` | Do not use the vars file in `vars/` |
| `DISABLE_COLOR` | `1` | Disable Terrafrom colour output |

## Example

### Deploy to test

This will read the following files for the Terrafrom backend and variables:

* `backend/test.backend`
* `vars/test.tfvars`

```bash
export AWS_PROFILE=your-test-profile
make init
make plan
make apply
```

### Deploy to prod

This will read the following files for the Terraform backend and variables:

* `backend/prod.backend`
* `vars/prod.tfvars`

```bash
export AWS_PROFILE=your-prod-profile
export ENVIRONMENT=prod
make init
make plan
make apply
```

