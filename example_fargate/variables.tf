variable "application" {
  type    = string
  default = "silvapache"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "name_prefix" {
  type = string
  default = "fargate_sidecar_3"
}

variable "task_definition_cpu" {
  type = string
  default = "512"
}

variable "task_definition_memory" {
  type = string
  default = "1024"
}
