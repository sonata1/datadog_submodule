module "datadog_sidecar" {
  # source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/datadog_submodule.git"
  source                = "../"
  application           = var.application     # REQUIRED FOR TAGGING AND LOG ROUTER CONFIGURATION FOR DATADOG
  name_prefix           = var.name_prefix
  datadog_logs_group    = "/ecs/${var.name_prefix}_fargate_service"
  container_definitions = local.container_definitions
  datadog_tags = {
    Name = var.application
  }
}

resource "aws_ecs_cluster" "fargate_cluster" {
  name = var.name_prefix

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_service" "fargate_service" {
  name                               = "${var.name_prefix}-service"
  cluster                            = aws_ecs_cluster.fargate_cluster.id
  task_definition                    = aws_ecs_task_definition.sidecar_task.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = ["sg-0f07acdd691fad29f"]
    subnets          = ["subnet-001cca369e9403d01"]
    assign_public_ip = true
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}

resource "aws_ecs_task_definition" "sidecar_task" {
  family                   = var.name_prefix
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.task_definition_cpu
  memory                   = var.task_definition_memory
  execution_role_arn       = aws_iam_role.ecs.arn
  task_role_arn            = aws_iam_role.ecs.arn
  container_definitions    = jsonencode((module.datadog_sidecar).container_definitions)
}

locals {
  container_definitions = [
    {
      name      = var.application,
      image     = "121548802581.dkr.ecr.eu-west-2.amazonaws.com/httpd:latest",
      essential = true,
      entryPoint = [
        "sh",
        "-c"
      ],
      portMappings = [
        {
          "protocol"      = "tcp",
          "containerPort" = 80,
          "hostPort"      = 80
        }
      ],
      command = [
        "/bin/sh -c \"echo '<html> <head> <title>Amazon ECS Sample App</title> <style>body {margin-top: 40px; background-color: #333;} </style> </head><body> <div style=color:white;text-align:center> <h1>Amazon ECS Sample App</h1> <h2>Congratulations!</h2> <p>Your application is now running on a container in Amazon ECS.</p> </div></body></html>' >  /usr/local/apache2/htdocs/index.html && httpd-foreground\""
      ],
      cpu    = 128,
      memory = 256,
      environment = [
        {
          name  = "DD_TAGS",
          value = "env:dev,service:${var.application}"
        }
      ]
    }
  ]
}
